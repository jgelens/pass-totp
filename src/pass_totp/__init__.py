__version__ = "(1, 0, 1, 'final', 0)"

__name__ = "pass_totp"
__description__ = "TOTP Extension for 'pass' (https://www.passwordstore.org)"
__uri__ = "https://gitlab.com/jgelens/pass-totp"

__author__ = "Jeffrey Gelens"
__email__ = "jeffrey@noppo.pro"

__license__ = "Apache 2"
__copyright__ = "Copyright (c) 2017 Noppo, Jeffrey Gelens"
