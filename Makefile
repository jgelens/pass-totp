help:
	@echo "Commands: setup, upload, cleanup"

setup:
	python setup.py sdist

upload:
	twine upload dist/*

cleanup:
	rm -rf dist build

release: cleanup setup upload
